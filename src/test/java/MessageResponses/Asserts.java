package MessageResponses;
import org.testng.asserts.SoftAssert;
import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.testng.Reporter;

public class Asserts {
	
	public static String verify(String response[],String msg[],int size) {
		List <String> pass=new ArrayList<String>();
		List <String> fail=new ArrayList<String>();
		String report=null;
		int n=0;
		
		SoftAssert soft= new SoftAssert();
		for(int i=0;i<size;i++) {
			int j=i;
			
				soft.assertEquals(response[i],msg[j]);
					if(response[i].contains(msg[j])) {
						
						pass.add("matched");
						
					}
					else {
						fail.add("not matched");
					}
			}
		if(fail.size()<=0) {
			report="PASS";
		}else {
			
			report="FAIL";
		}
		
		return report;
		
	
		
		
		
	}

}
