package MessageResponses;

import java.io.IOException;

import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import MessengerWeb.MessengerWeb.Reader;

public class Ministatement {
static Reader rd = new Reader();
	
	static SoftAssert as= new SoftAssert();
	
	public static void invalid(String response[]) throws IOException {
		int size=response.length;
		String msg1= rd.read(1,18,3);
		String msg2= rd.read(1,19,3);
		String msg[]= {msg1,msg2};
		Asserts.verify(response,msg,size);
		Reporter.log("invalid account scenario : "+Asserts.verify(response, msg, size),true);
	}
	public static void unlinked(String response[]) throws IOException {
		int size=response.length;
		String msg1=rd.read(1, 20, 3);
		String msg2=rd.read(1, 21, 3);
		String msg[]= {msg1,msg2};
		Asserts.verify(response, msg, size);
		Reporter.log("unlinked account scenario : "+Asserts.verify(response, msg, size),true);
	}
	public static void not_own(String response[]) throws IOException {
		int size=response.length;
		String msg1=rd.read(1, 22, 3);
		String msg2=rd.read(1, 23, 3);
		String msg[]= {msg1,msg2};
		Asserts.verify(response, msg, size);
		Reporter.log("not own account scenario : "+Asserts.verify(response, msg, size),true);
	}
	public static void correct(String response[]) throws IOException {
		int size=response.length;
		String msg1=rd.read(1, 24, 3);
		String msg2=rd.read(1, 25, 3);
		String msg3=rd.read(1, 26, 3);
		String msg[]= {msg1,msg2,msg3};
		Asserts.verify(response, msg, size);
		Reporter.log("correct account scenario : "+Asserts.verify(response, msg, size),true);
	}


}
