package MessageResponses;
import static org.testng.Assert.assertEquals;
import org.testng.asserts.SoftAssert;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.testng.Reporter;

import MessengerWeb.MessengerWeb.*;

public class Balance {
	static Reader rd = new Reader();
	
	static SoftAssert as= new SoftAssert();
	
	public static void invalid(String response[]) throws IOException {
		int size=response.length;
		String msg1= rd.read(1,9,3);
		String msg2= rd.read(1,10,3);
		String msg[]= {msg1,msg2};
		Asserts.verify(response,msg,size);
		Reporter.log("invalid account scenario : "+Asserts.verify(response, msg, size),true);
	}
	public static void unlinked(String response[]) throws IOException {
		int size=response.length;
		String msg1=rd.read(1, 11, 3);
		String msg2=rd.read(1, 12, 3);
		String msg[]= {msg1,msg2};
		Asserts.verify(response, msg, size);
		Reporter.log("unlinked account scenario : "+Asserts.verify(response, msg, size),true);
	}
	public static void not_own(String response[]) throws IOException {
		int size=response.length;
		String msg1=rd.read(1, 13, 3);
		String msg2=rd.read(1, 14, 3);
		String msg[]= {msg1,msg2};
		Asserts.verify(response, msg, size);
		Reporter.log("not own account scenario : "+Asserts.verify(response, msg, size),true);
	}
	public static void correct(String response[]) throws IOException {
		int size=response.length;
		String msg1=rd.read(1, 15, 3);
		String msg2=rd.read(1, 16, 3);
		String msg3=rd.read(1, 17, 3);
		String msg[]= {msg1,msg2,msg3};
		Asserts.verify(response, msg, size);
		Reporter.log("unlinked account scenario : "+Asserts.verify(response, msg, size),true);
	}

}
