package MessageResponses;

import java.io.IOException;

import org.testng.Reporter;

import MessengerWeb.MessengerWeb.Reader;

public class Airtime {
	static Reader rd = new Reader();
	
	
	public static void invalidmobile(String response[]) throws IOException {
		int size=response.length;
		String msg1=rd.read(1, 27, 3);
		String msg2=rd.read(1, 28, 3);
		String msg3=rd.read(1, 29, 3);
		String msg[]= {msg1,msg2,msg3};
		Asserts.verify(response, msg, size);
		Reporter.log(" Invalid mobile number scenario: "+Asserts.verify(response, msg, size), true);
		
	}
	public static void withcode(String response[]) throws IOException {
		int size=response.length;
		String msg1=rd.read(1, 30, 3);
		String msg2=rd.read(1, 31, 3);
		String msg3=rd.read(1, 32, 3);
		String msg4=rd.read(1, 33, 3);
		String msg5=rd.read(1, 34, 3);
		String msg6=rd.read(1, 35, 3);
		String msg[]= {msg1,msg2,msg3,msg4,msg5,msg6};
		Asserts.verify(response, msg, size);
		Reporter.log(" Mobile number with international code scenario: "+Asserts.verify(response, msg, size), true);
		
	}
	public static void normalairtel(String response[]) throws IOException {
		int size=response.length;
		String msg1=rd.read(1, 36, 3);
		String msg2=rd.read(1, 37, 3);
		String msg3=rd.read(1, 38, 3);
		String msg4=rd.read(1, 39, 3);
		String msg5=rd.read(1, 40, 3);
		String msg6=rd.read(1, 41, 3);
		String msg[]= {msg1,msg2,msg3,msg4,msg5,msg6};
		Asserts.verify(response, msg, size);
		Reporter.log(" Mobile number with international code scenario: "+Asserts.verify(response, msg, size), true);
		
		
	}
	public static void normalequitel(String response[]) throws IOException {
		int size=response.length;
		String msg1=rd.read(1, 42, 3);
		String msg2=rd.read(1, 43, 3);
		String msg3=rd.read(1, 44, 3);
		String msg4=rd.read(1, 45, 3);
		String msg5=rd.read(1, 46, 3);
		String msg6=rd.read(1, 47, 3);
		String msg[]= {msg1,msg2,msg3,msg4,msg5,msg6};
		Asserts.verify(response, msg, size);
		Reporter.log(" Mobile number with international code scenario: "+Asserts.verify(response, msg, size), true);
		
	}
	public static void limits(String response[]) {
		
	}

}
