package MessengerWeb.MessengerWeb;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Mpesa extends Launch {
	
	Universal universal = new Universal();
	
	public void initiate(String mobile[], String amounts[],String accounts[],String digits[]) throws InterruptedException {
		String request="Send to mpesa";
		invalidnumber(request, mobile);
		numberwithcode(request, mobile, amounts, accounts, digits);
		normalnumber(request, mobile, amounts, accounts, digits);
		AndClause(request, mobile, amounts, accounts, digits);
		AmountInWords(request, mobile, amounts, accounts, digits);
		invalidsource(request, mobile, amounts, accounts);
		unlinkedsource(request, mobile, amounts, accounts);
		canceltrans(request, mobile, amounts, accounts, digits);
	}
	public void invalidnumber(String request,String mobile[]) throws InterruptedException {
		universal.text(request);
		Thread.sleep(5000);
		universal.text(mobile[0]);
		Thread.sleep(5000);
		universal.text(mobile[0]);
	}
	public void numberwithcode(String request,String mobile[], String amounts[],String accounts[],String digits[]) throws InterruptedException {
		universal.text(request);
		Thread.sleep(5000);
		universal.text(mobile[1]);
		Thread.sleep(5000);
		universal.text(amounts[0]);
		Thread.sleep(5000);
		universal.text(accounts[3]);
		universal.proceed();
		universal.pin(digits);
		universal.confirmation();
	}
	public void normalnumber(String request,String mobile[], String amounts[],String accounts[],String digits[]) throws InterruptedException {
		universal.text(request);
		Thread.sleep(5000);
		universal.text(mobile[2]);
		Thread.sleep(5000);
		universal.text(amounts[1]);
		Thread.sleep(5000);
		universal.text(accounts[3]);
		universal.proceed();
		universal.pin(digits);
		universal.confirmation();
	}
	public void AndClause(String request,String mobile[], String amounts[],String accounts[], String digits[]) throws InterruptedException {
		universal.text(request);
		Thread.sleep(5000);
		universal.text(mobile[2]);
		Thread.sleep(5000);
		universal.text(amounts[2]);
		Thread.sleep(5000);
		universal.text(accounts[3]);
		universal.proceed();
		universal.pin(digits);
		universal.confirmation();
	}
	public void AmountInWords(String request,String mobile[], String amounts[],String accounts[], String digits[]) throws InterruptedException {
		universal.text(request);
		Thread.sleep(5000);
		universal.text(mobile[1]);
		Thread.sleep(5000);
		universal.text(amounts[1]);
		Thread.sleep(5000);
		universal.text(accounts[3]);
		universal.proceed();
		universal.pin(digits);
		universal.confirmation();
	}
	public void invalidsource(String request,String mobile[], String amounts[],String accounts[]) throws InterruptedException {
		universal.text(request);
		Thread.sleep(5000);
		universal.text(mobile[1]);
		Thread.sleep(5000);
		universal.text(amounts[0]);
		Thread.sleep(5000);
		universal.text(accounts[0]);
		
	}
	public void unlinkedsource(String request,String mobile[], String amounts[],String accounts[]) throws InterruptedException {
		universal.text(request);
		Thread.sleep(5000);
		universal.text(mobile[2]);
		Thread.sleep(5000);
		universal.text(amounts[0]);
		Thread.sleep(5000);
		universal.text(accounts[1]);
	}
	public void canceltrans(String request,String mobile[], String amounts[],String accounts[], String digits[]) throws InterruptedException {
		universal.text(request);
		Thread.sleep(5000);
		universal.text(mobile[1]);
		Thread.sleep(5000);
		universal.text(amounts[0]);
		Thread.sleep(5000);
		universal.text(accounts[3]);
		universal.proceed();
		universal.pin(digits);
		universal.Cancelconfirmation();
	}
	public void min_limit() {
		
	}
	public void max_limit() {
		
	}
	
}
