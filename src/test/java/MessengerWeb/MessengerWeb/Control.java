package MessengerWeb.MessengerWeb;

import java.io.IOException;

import org.testng.annotations.Test;
	// This class controls the text execution of various Chatbot services .
public class Control extends Launch {
	  
	Data data = new Data();
	
	@Test(priority= 1, enabled= false, description = "check balance test suite")
	 public void balance() throws IOException, InterruptedException {
		
		data.balance();
	}
	@Test(priority= 2, enabled= false, description = "transaction history test suite")
	public void statement() throws IOException, InterruptedException {
		data.ministatement();
	}
	@Test(priority= 3, enabled= false, description = "buy airtime test suite")
	public void airtime() throws InterruptedException, IOException {
		data.airtime();
	}
	@Test(priority= 4, enabled= false, description = "send to equity test suite")
	public void equity() throws IOException, InterruptedException {
		data.equity();
	}
	@Test(priority= 5, enabled= false, description = "send to mpesa test suite")
	public void mpesa() throws IOException, InterruptedException {
		data.mpesa();
	}
	@Test(priority= 5, enabled= true, description = "send to mpesa test suite")
	public void loans() throws IOException, InterruptedException {
		data.loans();
	}

}
