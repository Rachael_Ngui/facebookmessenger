package MessengerWeb.MessengerWeb;

public class Loans {
	
	Universal universal = new Universal();
	public void initiate(String[]loantype,String[] duration,String[] income,String[] job_description, String email, String mobile) throws InterruptedException {
		String start ="Loan Restructure";
		EazzyLoan(loantype, duration, income, job_description, email, mobile, start);
		EazzyLoanPlus(loantype, duration, income, job_description, email, mobile, start);
		BusinessCustomer(loantype, duration, income, job_description, email, mobile, start);
		EmployedCustomer(loantype, duration, income, job_description, email, mobile, start);
		EazzyLoan_MinDuration(loantype, duration, income, job_description, email, mobile, start);
		EazzyLoan_MaxDuration(loantype, duration, income, job_description, email, mobile, start);
		EazzyLoanPlus_MinDuration(loantype, duration, income, job_description, email, mobile, start);
		EazzyLoanPlus_MaxDuration(loantype, duration, income, job_description, email, mobile, start);
	}
	public void EazzyLoan(String[]loantype,String[] duration,String[] income,String[] job_description, String email, String mobile, String start) throws InterruptedException {
		universal.text(start);
		universal.text(loantype[0]); // 1 month loan
		universal.text(duration[1]); // 2 months
		universal.text(income[0]); //  employed
		universal.text(job_description[0]); // public
		universal.text(email);
		universal.text(mobile);
		Thread.sleep(5000);
		
	}
	public void EazzyLoanPlus(String[]loantype,String[] duration,String[] income,String[] job_description, String email, String mobile, String start) throws InterruptedException {
		universal.text(start);
		universal.text(loantype[1]); // installment loan
		universal.text(duration[3]); // 4 months
		universal.text(income[1]); //  business
		universal.text(job_description[2]); // retail trade
		universal.text(email);
		universal.text(mobile);
		Thread.sleep(5000);
	}
	public void MinimumAmount(String[]loantype,String[] duration,String[] income,String[] job_description, String email, String mobile) {
		
	}
	public void BusinessCustomer(String[]loantype,String[] duration,String[] income,String[] job_description, String email, String mobile, String start) throws InterruptedException {
		universal.text(start);
		universal.text(loantype[0]); // 1 month loan
		universal.text(duration[1]); // 2 months
		universal.text(income[1]); //  business
		universal.text(job_description[4]); // manufacturing
		universal.text(email);
		universal.text(mobile);
		Thread.sleep(5000);
	}
	public void EmployedCustomer(String[]loantype,String[] duration,String[] income,String[] job_description, String email, String mobile, String start) throws InterruptedException {
		universal.text(start);
		universal.text(loantype[1]); // installment loan
		universal.text(duration[5]); // 4 months
		universal.text(income[0]); //  business
		universal.text(job_description[1]); // private employee
		universal.text(email);
		universal.text(mobile);
		Thread.sleep(5000);
	}
	public void EazzyLoan_MinDuration(String[]loantype,String[] duration,String[] income,String[] job_description, String email, String mobile, String start) throws InterruptedException {
		universal.text(start);
		universal.text(loantype[0]); // 1 month loan
		universal.text(duration[0]); // 1 month
		//universal.text(income[0]); //  employed
		//universal.text(job_description[0]); // public
		//universal.text(email);
		//universal.text(mobile);
	}
    public void EazzyLoan_MaxDuration(String[]loantype,String[] duration,String[] income,String[] job_description, String email, String mobile, String start) throws InterruptedException {
    	universal.text(start);
		universal.text(loantype[0]); // 1 month loan
		universal.text(duration[3]); // 4 months
		//universal.text(income[0]); //  employed
		//universal.text(job_description[0]); // public
		//universal.text(email);
		//universal.text(mobile);
	}
	public void EazzyLoanPlus_MinDuration(String[]loantype,String[] duration,String[] income,String[] job_description, String email, String mobile, String start) throws InterruptedException {
		universal.text(start);
		universal.text(loantype[0]); // 1 month loan
		universal.text(duration[2]); // 4 months
	}
    public void EazzyLoanPlus_MaxDuration(String[]loantype,String[] duration,String[] income,String[] job_description, String email, String mobile, String start) throws InterruptedException {
    	universal.text(start);
		universal.text(loantype[0]); // 1 month loan
		universal.text(duration[6]); // 8 months
	}

}
