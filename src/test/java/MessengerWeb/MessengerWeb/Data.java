package MessengerWeb.MessengerWeb;

import java.io.IOException;

	// This class fetches data from the excel sheet and passes it to the specific action(feature) classes.
public class Data {
	 Reader rd = new Reader();
	 
	 public void balance() throws IOException, InterruptedException {
		 Balance bal= new Balance();
			String invalid= rd.read(0,6,1);
			String unlinked= rd.read(0,7,1);
			String notown= rd.read(0,8,1);
			String correct= rd.read(0,9,1);
			String d_one =rd.read(0, 29, 1);
			String d_two =rd.read(0, 30, 1);
			String d_three =rd.read(0, 31, 1);
			String d_four =rd.read(0, 32, 1);
			String digits[]= {d_one,d_two,d_three,d_four};
			String accounts[]= {invalid,unlinked,notown,correct};
			bal.balance(accounts,digits);	
	 }

	 public void ministatement() throws IOException, InterruptedException {
		 Ministatement ms= new Ministatement();
			String invalid= rd.read(0,6,1);
			String unlinked= rd.read(0,7,1);
			String notown= rd.read(0,8,1);
			String correct= rd.read(0,9,1);
			String d_one =rd.read(0, 29, 1);
			String d_two =rd.read(0, 30, 1);
			String d_three =rd.read(0, 31, 1);
			String d_four =rd.read(0, 32, 1);
			String digits[]= {d_one,d_two,d_three,d_four};
			String accounts[]= {invalid,unlinked,notown,correct};
			ms.data(accounts,digits);	
		
	}

	public void airtime() throws InterruptedException, IOException {
		Airtime airtime= new Airtime();
		String invalid= rd.read(0,6,1);
		String unlinked= rd.read(0,7,1);
		String notown= rd.read(0,8,1);
		String correct= rd.read(0,9,1);
		String safcom_no =rd.read(0,15, 1);
		String airtel_no =rd.read(0,16, 1);
		String equitel_no= rd.read(0,17, 1);
		String invalid_no =rd.read(0,14,1);
		String numeric =rd.read(0,20,1);
		String words = rd.read(0,21,1);
		String and_clause = rd.read(0,22,1);
		String minimum = rd.read(0, 23, 1);
		String maximum = rd.read(0, 24, 1);
		String equitel_min = rd.read(0, 25, 1);
		String d_one =rd.read(0, 29, 1);
		String d_two =rd.read(0, 30, 1);
		String d_three =rd.read(0, 31, 1);
		String d_four =rd.read(0, 32, 1);
		String digits[]= {d_one,d_two,d_three,d_four};
		String accounts[]= {invalid,unlinked,notown,correct};
		String mobile[]= {safcom_no,airtel_no,equitel_no,invalid_no};
		String amounts[]= {numeric,words,and_clause,minimum,maximum,equitel_min};
		airtime.initiate(accounts, mobile, amounts,digits);
				
	}

	public void equity() throws IOException, InterruptedException {
		SendToEquity eq= new SendToEquity();
		String invalid= rd.read(0,6,1);
		String unlinked= rd.read(0,7,1);
		String notown= rd.read(0,8,1);
		String correct= rd.read(0,9,1);
		String usd_ac = rd.read(0, 10, 1);
		String non_eq = rd.read(0, 11, 1);
		String numeric =rd.read(0,20,1);
		String words = rd.read(0,21,1);
		String and_clause = rd.read(0,22,1);
		String minimum = rd.read(0, 25, 1);
		String maximum = rd.read(0, 26, 1);
		String d_one =rd.read(0, 29, 1);
		String d_two =rd.read(0, 30, 1);
		String d_three =rd.read(0, 31, 1);
		String d_four =rd.read(0, 32, 1);
		String digits[]= {d_one,d_two,d_three,d_four};
		String accounts[]= {invalid,unlinked,notown,correct,usd_ac,non_eq};
		String amounts[]= {numeric,words,and_clause,minimum,maximum};
		eq.initiate(accounts, amounts,digits);
	}

	public void mpesa() throws IOException, InterruptedException {
		Mpesa mpesa = new Mpesa();
		String invalid= rd.read(0,6,1);
		String unlinked= rd.read(0,7,1);
		String notown= rd.read(0,8,1);
		String correct= rd.read(0,9,1);
		String usd_ac = rd.read(0, 10, 1);
		String non_eq = rd.read(0, 11, 1);
		String numeric =rd.read(0,20,1);
		String words = rd.read(0,21,1);
		String and_clause = rd.read(0,22,1);
		String minimum = rd.read(0, 25, 1);
		String maximum = rd.read(0, 26, 1);
		String d_one =rd.read(0, 29, 1);
		String d_two =rd.read(0, 30, 1);
		String d_three =rd.read(0, 31, 1);
		String d_four =rd.read(0, 32, 1);
		String invalid_no =rd.read(0,14,1);
		String withcode =rd.read(0,15, 1);
		String normal =rd.read(0,15, 2);
		String mobile[]= {invalid_no,withcode,normal};
		String digits[]= {d_one,d_two,d_three,d_four};
		String accounts[]= {invalid,unlinked,notown,correct,usd_ac,non_eq};
		String amounts[]= {numeric,words,and_clause,minimum,maximum};
		mpesa.initiate(mobile, digits, accounts, amounts);
	}
	public void loans() throws IOException, InterruptedException {
		Loans loans = new Loans();
		String eazzyloan = rd.read(0, 35, 1);
		String installment = rd.read(0, 36, 1);
		String one_month = rd.read(0, 39, 2);
		String two_months = rd.read(0, 39, 1);
		String three_months = rd.read(0, 40, 1);
		String four_months = rd.read(0, 41, 1);
		String five_months = rd.read(0, 42, 1);
		String six_months = rd.read(0, 43, 1);
		String eight_months = rd.read(0, 43, 2);
		String employed = rd.read(0, 46, 1);
		String business = rd.read(0, 47, 1);
		String civil = rd.read(0, 53, 1);
		String non_civil = rd.read(0, 54, 1);
		String retail = rd.read(0, 55, 1);
		String services = rd.read(0, 56, 1);
		String manufacturing = rd.read(0, 57, 1);
		String wholesale = rd.read(0, 58, 1);
		String email = rd.read(0, 50, 1);
		String mobile = rd.read(0, 17, 1);
		String[] loantype= {eazzyloan,installment};
		String[] duration = {one_month, two_months,three_months,four_months,five_months,six_months,eight_months};
		String[] income = {employed,business};
		String[] job_description = {civil,non_civil,retail,services,manufacturing,wholesale};
		loans.initiate(loantype,duration,income, job_description, email, mobile);
	}
}
