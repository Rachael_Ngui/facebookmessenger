package MessengerWeb.MessengerWeb;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Onboarding extends Launch{
	@Test(priority=1, enabled= true, description= "start conversation with the bot ")
	public void initiate() throws InterruptedException {
		WebElement texttab= driver.findElement(By.cssSelector(".notranslate"));
		texttab.sendKeys("hi");
		texttab.sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		texttab.sendKeys("balance");
		texttab.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		texttab.sendKeys("yes");
		texttab.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		WebElement chatscreen= driver.findElement(By.xpath("//*[@id=\"js_1\"]"));
		List<WebElement> messages= chatscreen.findElements(By.tagName("a"));
		int last=messages.size()-1;
		if (messages.get(last-1).getText()!=null) {
			Thread.sleep(3000);
			messages.get(last-1).click();
		}else {
			System.out.println("Button not found");
		}	
	}
	@Test(priority=2, enabled= true, description= "invalid account number (less / more) digits")
	public void invalidaccount() throws InterruptedException {
		for (int i=0; i<4;i++) {
			
			  if (i==0) { 
		      Thread.sleep(10000);
			  driver.findElement(By.tagName("iframe"));
			  driver.switchTo().frame("messenger_ref");
			  WebElement form =driver.findElement(By.tagName("form"));
			  WebElement account=form.findElement(By.id("accountNumber"));
			  account.click();
			  Thread.sleep(5000); 
			  account.sendKeys("3436546"); 
			  Thread.sleep(2000);
			  WebElement submit= driver.findElement(By.className("dynamic-form-control"));
			  submit.findElement(By.tagName("button")).click();
			  Thread.sleep(10000);
			  driver.switchTo().parentFrame();
			  WebElement close=
			  driver.findElement(By.xpath(
			  "//*[@id=\"facebook\"]/body/div[2]/div[2]/div/div/div/div/div"));
			  close.findElement(By.tagName("button")).click(); 
			  }
			 
		  if (i==1)
		  {
			    //initiate();
			    Thread.sleep(10000);
				driver.findElement(By.tagName("iframe"));
				driver.switchTo().frame("messenger_ref");
				WebElement form =driver.findElement(By.tagName("form"));
				WebElement account= form.findElement(By.id("accountNumber"));
				//Thread.sleep(3000);
				account.click();
				Thread.sleep(5000);
				account.sendKeys("1180163966239");
				Thread.sleep(2000);
				WebElement submit= driver.findElement(By.className("dynamic-form-control"));
				submit.findElement(By.tagName("button")).click();
				Thread.sleep(10000);
				WebElement idform = driver.findElement(By.tagName("form"));
				WebElement idnumber= idform.findElement(By.id("idcardnumber"));
				idnumber.click();
				idnumber.sendKeys("3486193");
				WebElement submitid= driver.findElement(By.className("dynamic-form-control"));
				submitid.findElement(By.tagName("button")).click(); 
				Thread.sleep(10000);
				driver.switchTo().parentFrame();
				WebElement close= driver.findElement(By.xpath("//*[@id=\"facebook\"]/body/div[2]/div[2]/div/div/div/div/div"));
				close.findElement(By.tagName("button")).click();
			  
		  }
		  if (i==2) {
			  
		  }
		  if (i==3) {
			  
		  }
			
		}
		
	}

}
