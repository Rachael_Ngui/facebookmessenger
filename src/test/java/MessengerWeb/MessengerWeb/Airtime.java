package MessengerWeb.MessengerWeb;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Airtime extends Launch{
	
	Universal universal= new Universal();
	
	public void initiate(String accounts[],String mobile[],String amounts[],String digits[]) throws InterruptedException, IOException {
		String start= "buy airtime";
		invalidmobile(start, accounts, mobile);
		withcode(start, accounts, digits, mobile, amounts);
		normalnumber(start, mobile, amounts, digits);
		limits(start, mobile, amounts, digits);
	}
	
	public void invalidmobile(String start,String accounts[],String mobile[]) throws InterruptedException, IOException {
		universal.text(start);
		Thread.sleep(5000);
		String res1= universal.lastmessage();
		universal.text(mobile[3]);
		Thread.sleep(5000);
		String res2=universal.lastmessage();
		universal.text(mobile[3]);
		Thread.sleep(5000);
		String res3=universal.lastmessage();
		String response[]= {res1,res2,res3};
		MessageResponses.Airtime.invalidmobile(response);
	}
	
	public void withcode(String start,String accounts[],String digits[],String mobile[],String amounts[]) throws InterruptedException, IOException {
		universal.text(start);
		Thread.sleep(5000);
		String res1= universal.lastmessage();
		universal.text(mobile[0]);
		Thread.sleep(5000);
		String res2= universal.lastmessage();
		safaricom();
		Thread.sleep(5000);
		String res3= universal.lastmessage();
		universal.text(amounts[0]);
		Thread.sleep(5000);
		String res4= universal.lastmessage();
		universal.aftertelco();
		Thread.sleep(3000);
		String res5=universal.lastmessage();
		universal.proceed();
		universal.pin(digits);
		universal.closelink();
		Thread.sleep(2000);
		String res6=universal.lastmessage();
		String response[]= {res1,res2,res3,res4,res5,res6};
		MessageResponses.Airtime.withcode(response);
		}
		
	public void normalnumber(String start,String mobile[],String amounts[],String digits[]) throws InterruptedException, IOException {
		
		for (int i=0;i<2;i++) {
			
			if (i==0) { 					// airtel number
				  universal.text(start);
				  Thread.sleep(3000);
				  String res1=universal.lastmessage();
				  universal.text(mobile[1]);
			      Thread.sleep(5000); 
			      String res2=universal.lastmessage();
			      airtel();
			      Thread.sleep(5000);
			      String res3=universal.lastmessage();
			      universal.text(amounts[0]);
			      Thread.sleep(5000);
			      String res4=universal.lastmessage();
			      universal.aftertelco();
			      Thread.sleep(5000);
			      String res5=universal.lastmessage();
			      universal.proceed();
				  universal.pin(digits);
				  universal.closelink();
				  Thread.sleep(3000);
				  String res6=universal.lastmessage();
				  String response[]= {res1,res2,res3,res4,res5,res6};
				  MessageResponses.Airtime.normalairtel(response);
			  }
			 
			if (i==1) {							//equitel number
				  universal.text(start);
				  Thread.sleep(3000);
				  String res1=universal.lastmessage();
				  universal.text(mobile[2]);
			      Thread.sleep(5000); 
			      String res2=universal.lastmessage();
			      equitel();
			      Thread.sleep(5000);
			      String res3=universal.lastmessage();
			      universal.text(amounts[0]);
			      Thread.sleep(5000);
			      String res4=universal.lastmessage();
			      universal.aftertelco();
			      Thread.sleep(5000);
			      String res5=universal.lastmessage();
			      universal.proceed();
				  universal.pin(digits);
				  universal.closelink();
				  Thread.sleep(3000);
				  String res6=universal.lastmessage();
				  String response[]= {res1,res2,res3,res4,res5,res6};
				  MessageResponses.Airtime.normalequitel(response);
			}
			
		}
	}
	public void limits(String start,String mobile[],String amounts[],String digits[]) {
		for(int i=0;i<2;i++) {
			if (i==0) {
				
			}
			if(i==1) {
				
			}
		}
		
	}
	public void safaricom() throws InterruptedException {
		WebElement telcos= driver.findElement(By.className("_a2e"));
		List<WebElement> telco= telcos.findElements(By.tagName("div"));
		System.out.println(telco.size());
		for (WebElement tel:telco) {
			if(tel.getText().contains("Safaricom")) {
				Thread.sleep(2000);
				tel.click();
				break;
			}	
		}
		  
	}
	public void airtel() throws InterruptedException {
		WebElement telcos= driver.findElement(By.className("_a2e"));
		List<WebElement> telco= telcos.findElements(By.tagName("div"));
		System.out.println(telco.size());
		for (WebElement tel:telco) {
			if(tel.getText().contains("Airtel")) {
				Thread.sleep(2000);
				tel.click();
				break;
			}	
		}
	}
	public void equitel() throws InterruptedException {
		WebElement telcos= driver.findElement(By.className("_a2e"));
		List<WebElement> telco= telcos.findElements(By.tagName("div"));
		System.out.println(telco.size());
		for (WebElement tel:telco) {
			if(tel.getText().contains("Equitel")) {
				Thread.sleep(2000);
				tel.click();
				break;
			}	
		}
		
	}
	
	
	

}
