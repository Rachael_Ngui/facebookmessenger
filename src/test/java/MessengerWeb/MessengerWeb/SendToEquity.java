package MessengerWeb.MessengerWeb;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class SendToEquity extends Launch {
	
	Universal universal= new Universal();
	
	public void initiate(String accounts[],String amounts[],String digits[]) throws InterruptedException {
		String start ="send to equity";
		invalidDestination(accounts, start);
		invalidwithAmount(accounts, start, amounts);
		success(accounts, start, amounts, digits);
		crosscurrency(accounts, start, amounts, digits);
		wordsamount(accounts, start, amounts, digits);
		invalidsource(accounts, start, amounts, digits);
		unlinkedsource(accounts, start, amounts, digits);
		othersource(accounts, start, amounts, digits);
	}
	
	public void invalidDestination(String accounts[],String start) throws InterruptedException {
		universal.text(start);
		Thread.sleep(5000);
		universal.text(accounts[0]);
		Thread.sleep(5000);
		universal.text(accounts[0]);
	}
	
	public void invalidwithAmount(String accounts[],String start,String amounts[]) throws InterruptedException {
		universal.text(start);
		Thread.sleep(5000);
		universal.text(accounts[0]);
		Thread.sleep(5000);
		universal.text(amounts[1]); //amount in words
		
	}
	public void success(String accounts[],String start,String amounts[],String digits[]) throws InterruptedException {
		universal.text(start);
		Thread.sleep(5000);
		universal.text(accounts[0]);
		Thread.sleep(5000);
		universal.text(amounts[0]); // numeric amounts
		Thread.sleep(5000);
		universal.text(accounts[3]);
		universal.proceed();
		universal.pin(digits);
	}
	
	public void crosscurrency(String accounts[],String start,String amounts[],String digits[]) throws InterruptedException {
		universal.text(start);
		Thread.sleep(5000);
		universal.text(accounts[2]);
		Thread.sleep(5000);
		universal.text(amounts[0]); 
		Thread.sleep(5000);
		universal.text(accounts[3]);
		universal.proceed();
		universal.pin(digits);
				
	}
	
	public void wordsamount(String accounts[],String start,String amounts[],String digits[]) throws InterruptedException{
		universal.text(start);
		Thread.sleep(5000);
		universal.text(accounts[2]);
		Thread.sleep(5000);
		universal.text(amounts[1]); //amounts in words
		Thread.sleep(5000);
		universal.text(accounts[3]);
		universal.proceed();
		universal.pin(digits);
	}
	public void invalidsource(String accounts[],String start,String amounts[],String digits[]) throws InterruptedException {
		universal.text(start);
		Thread.sleep(5000);
		universal.text(accounts[2]);
		Thread.sleep(5000);
		universal.text(amounts[0]); 
		Thread.sleep(5000);
		universal.text(accounts[0]);
				
	}
	
	public void unlinkedsource(String accounts[],String start,String amounts[],String digits[]) throws InterruptedException {
		universal.text(start);
		Thread.sleep(5000);
		universal.text(accounts[2]);
		Thread.sleep(5000);
		universal.text(amounts[0]); 
		Thread.sleep(5000);
		universal.text(accounts[1]);
		
	}
	public void othersource(String accounts[],String start,String amounts[],String digits[]) throws InterruptedException {
		universal.text(start);
		Thread.sleep(5000);
		universal.text(accounts[2]);
		Thread.sleep(5000);
		universal.text(amounts[0]); 
		Thread.sleep(5000);
		universal.text(accounts[2]);
		
	}
	public void min_limit() {
		
	}
	public void max_limit() {
		
	}

}
