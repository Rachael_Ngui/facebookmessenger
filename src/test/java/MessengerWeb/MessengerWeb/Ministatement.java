package MessengerWeb.MessengerWeb;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Ministatement extends Launch{
	
	Universal universal= new Universal();
	
	public void data(String accounts[],String digits[]) throws InterruptedException, IOException {
		String start="mini statement";
		invalidaccount(accounts, start);
		unlinked(accounts,start);
		not_own(accounts,start);
		correctaccount(accounts,start,digits);
		
	}
	
	public void invalidaccount(String accounts[],String start) throws InterruptedException, IOException {
		universal.text(start);
		Thread.sleep(5000);
		String res1=universal.lastmessage();
		universal.text(accounts[0]);
		Thread.sleep(5000);
		String res2=universal.lastmessage();
		String response[]= {res1,res2};
		MessageResponses.Ministatement.invalid(response);
	}
	
	public void unlinked(String accounts[],String start) throws InterruptedException, IOException {
		universal.text(start);
		Thread.sleep(5000);
		String res1=universal.lastmessage();
		universal.text(accounts[1]);
		Thread.sleep(5000);
		String res2=universal.lastmessage();
		String response[]= {res1,res2};
		MessageResponses.Ministatement.unlinked(response);
	}
	
	public void not_own(String accounts[],String start) throws InterruptedException, IOException {
		universal.text(start);
		Thread.sleep(5000);
		String res1=universal.lastmessage();
		universal.text(accounts[2]);
		Thread.sleep(5000);
		String res2=universal.lastmessage();
		String response[]= {res1,res2};
		MessageResponses.Ministatement.not_own(response);
	}
	
	public void correctaccount(String accounts[],String start,String digits[]) throws InterruptedException, IOException {
		universal.text(start);
		Thread.sleep(5000);
		String res1=universal.lastmessage();
		universal.text(accounts[3]);
		Thread.sleep(5000);
		String res2=universal.lastmessage();
		universal.proceed();
		universal.pin(digits);
		universal.closelink();
		String res3=universal.lastmessage();
		String response[]= {res1,res2,res3};
		MessageResponses.Ministatement.correct(response);
	
	}
	
}

