package MessengerWeb.MessengerWeb;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class Universal extends Launch {

	public void text(String msg) throws InterruptedException {
		Thread.sleep(5000);
		WebElement texttab = driver.findElement(By.cssSelector(".notranslate"));
		texttab.click();
		texttab.sendKeys(msg);
		texttab.sendKeys(Keys.ENTER);
	}

	public void proceed() throws InterruptedException {
		Thread.sleep(10000);
		WebElement chatscreen = driver.findElement(By.xpath("//*[@id=\"js_1\"]"));
		List<WebElement> messages = chatscreen.findElements(By.tagName("a"));
		int last = messages.size() - 1;
		if (messages.get(last - 1).getText() != null) {
			Thread.sleep(3000);
			messages.get(last - 1).click();
		} else {
			System.out.println("Button not found");
		}
	}

	public void pin(String digits[]) throws InterruptedException {
		int i = 0;
		Thread.sleep(30000);
		driver.switchTo().frame("messenger_ref"); // switching to the iframe for the pin pop up
		WebElement pop_up = driver.findElement(By.tagName("app-dynamic-pin-pad"));
		WebElement pad = pop_up.findElement(By.cssSelector("div.pad"));
		System.out.println("fjhfyhgfhgc " + pad.getCssValue("class"));
		Thread.sleep(3000);
		List<WebElement> buttons = pad.findElements(By.tagName("div"));
		System.out.println(buttons.size());
		for (WebElement pin : buttons) {

			if (pin.getText().contains("digits[0]")) {
				System.out.println(pin.getText());
				pin.click();
			}
		}

		for (WebElement pin : buttons) {
			if (pin.getText().contains("digits[1]")) {
				System.out.println(pin.getText());
				pin.click();
			}
		}

		for (WebElement pin : buttons) {
			if (pin.getText().contains("digits[2]")) {
				System.out.println(pin.getText());
				pin.click();
			}
		}

		for (WebElement pin : buttons) {
			if (pin.getText().contains("digits[3]")) {
				System.out.println(pin.getText());
				pin.click();
			}
		}

	}

	public String lastmessage() {
		WebElement chatscreen = driver.findElement(By.xpath("//*[@id=\"js_1\"]"));
		List<WebElement> chat = chatscreen.findElements(By.tagName("div"));
		int last = chat.size() - 3;
		String value = chat.get(last).getText();
		return value;
	}

	public void closelink() throws InterruptedException {
		Thread.sleep(10000);
		driver.switchTo().parentFrame();
		WebElement close = driver.findElement(By.xpath("//*[@id=\"facebook\"]/body/div[2]/div[2]/div/div/div/div/div"));
		close.findElement(By.tagName("button")).click();
	}

	public void confirmation() throws InterruptedException {
		Thread.sleep(10000);
		WebElement confirm = driver.findElement(By.className("card-group"));
		List<WebElement> buttons = confirm.findElements(By.tagName("button"));
		System.out.println(buttons.size());
		for (WebElement value : buttons) {

			if (value.getText().contains("Confirm")) {
				value.click();
			}
		}
	}

	public void Cancelconfirmation() throws InterruptedException {
		Thread.sleep(10000);
		WebElement confirm = driver.findElement(By.className("card-group"));
		List<WebElement> buttons = confirm.findElements(By.tagName("button"));
		System.out.println(buttons.size());
		for (WebElement value : buttons) {
			if (value.getText().contains("Cancel")) {
				value.click();
			}
		}

	}

	public void aftertelco() throws InterruptedException {
		WebElement account = driver.findElement(By.className("_a28"));
		account.findElement(By.className("_10-e")).click();
		Thread.sleep(3000);

	}
	/*
	 * public String waiting() throws InterruptedException { WebElement chatscreen=
	 * driver.findElement(By.xpath("//*[@id=\"js_1\"]")); List<WebElement> chats=
	 * chatscreen.findElements(By.tagName("div")); int last= chats.size()-2; String
	 * sender = chats.get(last).getText(); for(WebElement chat:chats) {
	 * System.out.println(chat.getText()); }
	 * 
	 * while(!sender.contains("Equity")) { Thread.sleep(2000); }
	 * 
	 * return sender; }
	 */

}
