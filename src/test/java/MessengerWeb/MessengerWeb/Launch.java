package MessengerWeb.MessengerWeb;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Launch {
	static WebDriver driver;
	 @Test
	public static void driverlaunch() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "/home/rachael/eclipse-workspace/MessengerWeb/driver/chromedriver");
		driver = new ChromeDriver();
		driver.get("https://www.messenger.com/login.php");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		Thread.sleep(2000);
		String url= driver.getCurrentUrl();
		System.out.println("the testing application URL is: "+url);
		driver.findElement(By.id("email")).sendKeys("+254719695047");
		driver.findElement(By.id("pass")).sendKeys("kathiningui");
		driver.findElement(By.id("loginbutton")).click();
		Thread.sleep(2000);
		// finding the list elements
		WebElement chats= driver.findElement(By.xpath("//*[@id=\"js_u\"]/div/div/div[2]/div/ul"));
		List<WebElement> options=chats.findElements(By.tagName("li"));
		// iterating through the list of chats to find Equity Bank Dev
		for(WebElement element : options) {
		if(element.getText().contains("Equity Bank Group Dev")) {
			element.click();
		    }
		  }
		
	  
			}

}
